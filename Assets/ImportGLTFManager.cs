﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Siccity.GLTFUtility;
using UnityEngine.Networking;
using System.IO;
using System;

public class ImportGLTFManager : MonoBehaviour
{
	public string url;


	private void Start()
    {
		Action callBack = () =>
		{
			GameObject result = Importer.LoadFromFile("C:/Users/Aunkon/Desktop/Melon260.glb");
		};
		StartCoroutine(LoadModelFromOnlineAsync(callBack: callBack));
    }


	private IEnumerator LoadModelFromOnlineAsync(Action callBack)
	{
		using (UnityWebRequest uwr = UnityWebRequest.Get(url))
		{
			Debug.Log("Downloading Model From Online");
			yield return uwr.SendWebRequest();
			if (uwr.isNetworkError || uwr.isHttpError)
			{
				Debug.LogWarning("Networking Problem");
			}
			else
			{
				File.WriteAllBytes(string.Format("C:/Users/Aunkon/Desktop/Melon260.glb"), uwr.downloadHandler.data);
				Debug.Log("Saving Complete Now Loading from local");
				yield return new WaitForSeconds(1.0f);
				callBack?.Invoke();
			}
		}
	}
}